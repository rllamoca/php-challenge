<?php

namespace App;

class Call
{
	protected $id_call;
	public $from_phone;
	public $destiny_phone;
	use Traits\Phone;

	function __construct($id_call = null, $destiny_phone = null)
	{
		$this->id_call = $id_call;
		$this->destiny_phone = $destiny_phone;
		# code...
	}

	public function getDestinyNumbersPhone(){
		return $this->getNumbersPhone($this->destiny_phone);
	}
}