<?php

namespace App;

use App\Interfaces\WhatsappInterface;
use App\Interfaces\CarrierInterface;
use App\Interfaces\SmsInterface;

use App\Services\ContactService;


class Mobile implements CarrierInterface, SMSInterface, WhatsappInterface
{

	function __construct()
	{
	}

	public function dialContact(Contact $contact){
//		echo "Llamando al número ".$contact->phone;
	}

	public function makeCall(): Call{
		return new Call();
	}

	public function sendSMS(Contact $contact, $message): Sms{
		return new Sms(rand(), $contact->phone, $message);
	}

	public function sendSMSByNumber($number, $message){
		if( empty($number) || empty($message) ) return;

		$contact = ContactService::findByNumber($number);

		if($contact->phone != null ){
			$this->sendSMS($contact, $message);
			return ContactService::validateNumber($contact->phone);
//			return $this->makeCall();
		}
		return;
	}

	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		$contact = ContactService::findByName($name);

		if($contact->phone != null ){
			$this->dialContact($contact);

			return $this->makeCall();
		}
		return;
	}

	public function lock(Contact $contact){}

	public function sendVideo(Contact $contact, $media){}

	public function sendMessage(Contact $contact, $message){}

    public function uploadStatus($media, $privacy){}

}
