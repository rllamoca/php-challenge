<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	
	public static function findByName(string $name): Contact
	{
		$contacts = ContactService::getAllContact();
		$neededContacts = array_filter(
			$contacts,
			function ($e) use (&$name) {
				return $e->name == $name;
			}
		);
		$neededContacts = array_values($neededContacts);
		if(sizeof($neededContacts) > 0){
			return $neededContacts[0];
		}
		else{
			return new Contact();
		}
	}

	public static function findByNumber(string $number): Contact
	{
		$contacts = ContactService::getAllContact();
		$neededContacts = array_filter(
		$contacts,
			function ($e) use (&$number) {
				return $e->phone == $number;
			}
		);
		$neededContacts = array_values($neededContacts);
		if(sizeof($neededContacts) > 0){
			return $neededContacts[0];
		}
		else{
			return new Contact();
		}
	}

	public static function getAllContact(){
		return ContactService::randomContacts(10);
	}

	public static function randomContacts($max){
		$contacts = array();
		for($x = 0; $x<$max; $x++){
			array_push($contacts, new Contact($x+1, ContactService::randomName($x), ContactService::randomPhone($x)));
		}
		return $contacts;
	}

	public static function randomName($index){
		$names = array(
			'ADRIANA CAROLINA HERNANDEZ MONTERROZA',
			'ADRIANA MARCELA REY SANCHEZ',
			'ALEJANDRO ABONDANO ACEVEDO',
			'ALEXANDER CARVAJAL VARGAS',
			'ANDREA CATALINA ACERO CARO',
			'ANDREA LILIANA CRUZ GARCIA',
			'ANDRES FELIPE VILLA MONROY',
			'ANGELA PATRICIA MAHECHA PIÑEROS',
			'ANGELICA LISSETH BLANCO CONCHA',
			'ANGELICA MARIA ROCHA GARCIA',
			'ANGIE TATIANA FERNÁNDEZ MARTÍNEZ',
			'BRIGITE POLANCO RUIZ',
			'CAMILO VILLAMIZAR ARISTIZABAL',
			'CAMILO RODRÍGUEZ BOTERO',
			'CAMILO ALBERTO CORTÉS MONTEJO',
			'CAMILO ENRIQUE GOMEZ RODRIGUEZ',
			'CARLOS ANDRÉS POLO CASTELLANOS',
			'CARLOS DIDIER CASTAÑO CONTRERAS',
			'CARLOS FELIPE MOGOLLÓN PACHÓN',
			'CAROL RUCHINA GOMEZ GIANINE',
			'CAROLINA PINTOR PINZON',
			'CATHERINE OSPINA ALFONSO',
			'CINTHYA FERNANDA DUSSÁN GUZMÁN',
			'CLAUDIA LILIANA TORRES FRIAS',
			'CRISTINA ELIZABETH BARTHEL GUARDIOLA',
			'DANIEL GÓMEZ DELGADO',
			'DANIEL ANDRÉS CASTIBLANCO SALGADO',
			'DANIELA HERNÁNDEZ BRAVO',
			'DANIELA GUZMÁN',
			'DANIELA KATHERINNE SUARIQUE ÁVILA',
			'DANIELLA PUERTO NAVIA',
			'DENY MARCELA MUÑOZ LIZARAZO',
			'DIANA CAROLINA LOPEZ RODRIGUEZ',
			'DIANA CATALINA DIAZ BELTRAN'	
		);
		try{
			return $names[$index];
		}
		catch(\Exception $e){
			return $names[rand ( 0 , count($names) -1)];
		}
	}

	public static function randomPhone($index){
		$numbers = array(
			'(460) 880-4668',
			'(500) 431-4148',
			'(977) 811-6231',
			'(999) 943-8528',
			'(922) 495-0798',
			'(977) 299-3136',
			'(977) 677-8052',
			'(384) 296-1966',
			'(764) 979-6813',
			'(327) 926-0425',
			'(920) 916-3393',
			'(860) 329-1729',
			'(808) 536-8365',
			'(745) 408-2991',
			'(642) 490-0591',
			'(262) 686-8760',
			'(279) 794-5537',
			'(520) 472-6528',
			'(295) 632-2944',
			'(804) 438-5683',
			'(742) 884-9792',
			'(221) 624-3354',
			'(768) 838-6634',
			'(488) 835-1415',
			'(771) 248-1293',
			'(738) 709-2422',
			'(901) 812-1048',
			'(415) 408-1615',
			'(352) 567-0407',
			'(625) 518-1424',
			'(421) 358-1271',
			'(663) 870-7218',
			'(540) 434-7679',
			'(248) 493-0744',			
		);
		try{
			return $numbers[$index];
		}
		catch(\Exception $e){
			return $numbers[rand ( 0 , count($numbers) -1)];
		}
	}

	public static function validateNumber(string $number): bool
	{
		$contact = ContactService::findByNumber($number);
		return (bool) $contact->phone;
		// logic to validate numbers
	}
	
}