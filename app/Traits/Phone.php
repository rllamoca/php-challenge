<?php

namespace App\Traits;

use App\Contact;
use App\Call;

trait Phone
{
    
	public function getNumbersPhone($number){
		if($number != null)
			return (int) filter_var($number, FILTER_SANITIZE_NUMBER_INT);		
		return 0;
	}

}