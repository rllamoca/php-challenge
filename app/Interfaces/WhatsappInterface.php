<?php

namespace App\Interfaces;

use App\Contact;
use App\Call;

interface WhatsappInterface
{
	
	public function lock(Contact $contact);

	public function sendVideo(Contact $contact, $media);

	public function sendMessage(Contact $contact, $message);

    public function uploadStatus($media, $privacy);

}