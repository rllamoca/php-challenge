<?php

namespace App\Interfaces;

use App\Contact;
use App\Call;

interface SmsInterface
{
	
	public function sendSMS(Contact $contact, $message);

}