<?php

namespace App;


class Sms
{
	protected $id_message;
	protected $from_phone;
    public $destiny_phone;
    public $message;

	function __construct($id_message = null, $destiny_phone = null, $message = null)
	{
		$this->id_message = $id_message;
		$this->destiny_phone = $destiny_phone;
		$this->message = $message;
	}

    public function getDestinyNumbersPhone(){
		return $this->getNumbersPhone($this->destiny_phone);
	}

}