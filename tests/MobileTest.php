<?php

namespace Tests;

use App\Mobile;
use App\Call;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$mobile = new Mobile();

		$this->assertNull($mobile->makeCallByName(''));
	}

	/** @test */
	public function make_call_by_name_valid_contact()
	{
		$mobile = new Mobile();

		$this->assertInstanceOf(Call::class,$mobile->makeCallByName('ADRIANA CAROLINA HERNANDEZ MONTERROZA'));
	}

	/** @test */
	public function make_call_by_name_valid_phone()
	{
		$mobile = new Mobile();

		$this->assertIsNumeric($mobile->makeCallByName('ADRIANA CAROLINA HERNANDEZ MONTERROZA')->getDestinyNumbersPhone());
	}

	/** @test */
	public function send_sms_number()
	{
		$mobile = new Mobile();

		$this->assertTrue($mobile->sendSMSByNumber('(460) 880-4668','Hola a todos'));
	}
	
}
